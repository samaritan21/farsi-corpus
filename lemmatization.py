#!/usr/bin/env python3

from __future__ import unicode_literals

import pandas as pd
import numpy as np
import lemm_functions as lemm

base_add = "/home/amir/simul/NLP/NLPK/"

# you can download Uppsala Persian Corpus (UPC) from here: http://stp.lingfil.uu.se/~mojgan/UPC.html
name="train.conll"
file_name=base_add+name
data=pd.read_csv(file_name, sep="\t", header=None)
#conllu_lab=["ID","FORM","LEMMA","UPOSTAG","XPOSTAG","FEATS","HEAD","DEPREL","DEPS","MISC"]
tags=np.sort(data[4].unique())
tags_lemm=["CON","P","ADV_TIME","ADV","ADV_COMP","ADV_I","ADV_LOC","ADV_NEG","DET","PRO"]
tags_ignore=["DELM","FW","INT","NUM","PREV","SYM","N_VOC","ADJ_VOC"]

#insert the lemmas for the tags that don't need any scraping
ind_lemm =[ data.index[data[4]==j] for j in tags_lemm]
ind_lemm=[item for sublist in ind_lemm for item in sublist]

data[2][ind_lemm]=data[1][ind_lemm]


blank=[] # save instances that couldn't be lemmatized
index=[]
for i in range(len(data)):
    tag=data[4][i]
    print(i)
    if  tag not in tags_ignore and tag not in tags_lemm:
        word=data[1][i]
        #find the right function to use
        if tag=="N_SING":
            lemma,special=lemm.N_SING(word,tag)
            if special==False:
               data[2][i]=lemma
        elif tag=="V_PP":
            lemma,special=lemm.V_PP(word,tag)
            if special==False:
               data[2][i]=lemma
        elif tag=="ADJ_CMPR":
            lemma,special=lemm.ADJ_CMPR(word,tag)
            if special==False:
               data[2][i]=lemma
               index.append(i)
        else:
           blank.append(i)

data.to_csv(base_add+"test_lemm.conll", sep="\t", header=None)
