# Farsi corpus

`lemmatization.py` finds the lemma for each word in the Uppsala Persian Corpus 
(UPC).

It utilizes vajehyab online dictionary ("https://www.vajehyab.com/") to find 
the POS tag of each word.

UPC is poblished here under GNU General Public License:
http://stp.lingfil.uu.se/~mojgan/UPC.html

# Contributors:
Amir Azizi

Olya Hakobyan