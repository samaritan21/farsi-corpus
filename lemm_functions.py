#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import requests
import bs4

def lookup(word,tag):
    rightside=["N_SING","ADJ_CMPR"]
    base_url = "https://www.vajehyab.com/?q="
    page = requests.get(base_url+word)
    soup = bs4.BeautifulSoup(page.content, "html.parser")
#    html = list(soup.children)
    item2 = soup.find(id="content")
    sub_item3 = item2.find(id="wordbox")
    mean = sub_item3.find(class_="mean")#.get_text()
    sub_item = item2.find(id="wordbox")
    if tag in rightside:
        item = soup.find(id="rightside")
        mean = item.find(class_="info").get_text()
        dic = sub_item.find(class_="dictionary").get_text()
    else:
        dic=[]
    return mean, dic

def N_SING(word,tag,special=False):
    endings=['ی', 'م', 'ت', 'ش', 'مان', 'تان', 'شان']
    cond=True
    t=0
    var=word
    while cond:
        pos,trans=lookup(var,tag)
        cond= re.findall('اسم', pos)==[]
        if not cond:
            break
        dum=[re.findall(e+'$', var) for e in endings]
        a=[x for x in dum if x != []]
        if len(a)!=0:
            dum=[x for x in dum if x!=[]][0][0]
            var=re.sub(dum+'$','',var)
            t=t+1
            if t>1:
                special=True
                break
        else:
            special=True
            break
    return var, special

def V_PP(word,tag,special=False):
    endings=['ه‌اند', 'ه‌اید', 'ه‌ایم', 'ه‌ام', 'ه‌ای', 'ه', 'یم', 'ید', 'ند', 'م', 'ی']
    begin=['نمی', 'می', 'ن']
    cond=True
    t=0
    var=word
    while  cond:
        if t==0:
            var2=var[:-1]+'ن'
        else:
            var2=var+'ن'
        mean,_=lookup(var2,tag)
        cond= str(mean)=='None'
        if not cond:
            break
        else:
            if t==1:
               var='ن'+var
            else:
                dum1=[re.findall('^'+e, var) for e in begin]
                dum1=[x for x in dum1 if x!=[]]
                if len(dum1)!=0:
                    aa=dum1[0][0]
                    if aa in begin[:2]:
                        aa=aa+'\u200c'
                    var=re.sub('^'+aa,'',var)
                dum=[re.findall(e+'$', var) for e in endings]
                dum=[x for x in dum if x!=[]]
                if len(dum)!=0:
                    var=re.sub(dum[0][0]+'$','',var)
                var2=var
            t=t+1
            if t>1:
                special=True
                break
    return var2, special

def ADJ_CMPR(word,tag,special=False):
    endings=['تری', 'ترم', 'ترت', 'ترش', 'ترمان', 'ترتان', 'ترشان','تر' ]
    cond=True
    t=0
    var=word
    while cond:
        pos,trans=lookup(var,tag)
        cond= re.findall('صفت', pos)==[]
        if not cond:
            break
        dum=[re.findall(e+'$', var) for e in endings]
        a=[x for x in dum if x != []]
        if len(a)!=0:
            dum=[x for x in dum if x!=[]][0][0]
            var=re.sub(dum+'$','',var)
            t=t+1
            if t>1:
                special=True
                break
        else:
            special=True
            break
    return var, special
